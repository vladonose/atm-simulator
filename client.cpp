#include <fstream>
#include <ctime>
#include <chrono>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 

#define BUF_LEN     40
#define INSF_ARGS   1
#define CALL_ERR    -1

int main(int argc, char *argv[]) {
    char adj[20];
    char log_name[40];

    auto raw_created = std::chrono::system_clock::now();
    std::time_t created_at = std::chrono::system_clock::to_time_t(raw_created);
    strftime(adj, sizeof(adj), "%Y%m%d%X", localtime(&created_at));
    sprintf(log_name, "client_logfile%s", adj);

    FILE *log_cl = fopen(log_name, "wb");
    fprintf(log_cl, "Client logfile created at: %s\n", std::ctime(&created_at));
    if (argc < 3) {
       printf("Incorrect call;\nTry: %s <server_ip> <server_port>", argv[0]);
       fprintf(log_cl, "Incorrect call;\nTry: %s <server_ip> <server_port>", argv[0]);
       fclose(log_cl);
       exit(INSF_ARGS);
    }

    int sock_udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    int sock_tcp = socket(AF_INET, SOCK_STREAM, 0);

    if (sock_tcp < 0) {
        printf("Error at opening socket\n");
        fprintf(log_cl, "Error at opening socket\n");
        fclose(log_cl);
        exit(CALL_ERR);
    }

    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2]));
    inet_aton(argv[1], &(serv_addr.sin_addr));

    struct sockaddr_in serv_udp;
    serv_udp.sin_family = AF_INET;
    serv_udp.sin_port = htons(atoi(argv[2]));
    serv_udp.sin_addr.s_addr = INADDR_ANY;

    if (connect(sock_tcp, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
        printf("Error at connect\n");
        fprintf(log_cl, "Error at connect\n");
        fclose(log_cl);
        exit(CALL_ERR);
    }

    fd_set read_fds;
    fd_set tmp_fds;
    FD_ZERO(&read_fds);
    FD_ZERO(&tmp_fds);
    FD_SET(0, &read_fds);
    FD_SET(sock_udp, &read_fds);
    FD_SET(sock_tcp, &read_fds);
    int fdmax = sock_tcp;

    char buffer[BUF_LEN];
    bool att_log = false;
    char card_n[7];
    unsigned int len = sizeof(struct sockaddr_in);

    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 10;
    setsockopt(sock_udp, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

    bool logged = false;

    for (;;) {
        tmp_fds = read_fds;
        if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) {
            printf("Error at select\n");
            fprintf(log_cl, "Error at select\n");
            fclose(log_cl);
            exit(CALL_ERR);
        }
        if (FD_ISSET(sock_tcp, &tmp_fds)) {
            if (recv(sock_tcp, buffer, sizeof(buffer), 0) <= 0) {
                printf("Error at recv\n");
                fprintf(log_cl, "Error at recv\n");
                fclose(log_cl);
                exit(CALL_ERR);
            }
            if (strncmp(buffer, "Server is shutting down", 23) == 0) {
                printf("%s", buffer);
                fprintf(log_cl, "%s", buffer);
                fclose(log_cl);
                return 1;
            } else if (strncmp(buffer, "IBANK> Welcome", 14) == 0) {
                logged = true;
            }
            printf("%s", buffer);
            fprintf(log_cl, "%s", buffer);
        }
        if (FD_ISSET(0, &tmp_fds)) {
            fflush(stdin);
            memset(buffer, 0 , BUF_LEN);
            fgets(buffer, BUF_LEN, stdin);
            fprintf(log_cl, "Client command: %s", buffer);
            if (strncmp(buffer, "quit", 4) == 0 && strlen(strtok(buffer, " ")) == 5) {
                int wv = send(sock_tcp, buffer, strlen(buffer) + 1, 0);
                if (wv < 0) {
                    printf("Error at send\n");
                    fprintf(log_cl, "Error at send\n");
                    fclose(log_cl);
                    exit(CALL_ERR);
                }
                return 0;
            } else if (strncmp(buffer, "login", 5) == 0) {
                if (!logged) {
                    if (send(sock_tcp, buffer, strlen(buffer) + 1, 0) < 0) {
                        printf("Error at send\n");
                        fprintf(log_cl, "Error at send\n");
                        fclose(log_cl);
                        exit(CALL_ERR);
                    }
                    char *split = strtok(buffer, " ");
                    split = strtok(NULL, " ");
                    if (split != NULL) {
                        memset(card_n, 0, 7);
                        strncpy(card_n, split, 6);
                        att_log = true;
                    }
                } else {
                    printf("-2 : Sesiune deja deschisa\n");
                    fprintf(log_cl, "-2 : Sesiune deja deschisa\n");
                }
            } else if (strcmp(buffer, "unlock\n") == 0) {
                if (att_log) {
                    sprintf(buffer + strlen(buffer), " %s\n", card_n);
                    sendto(sock_udp, buffer, sizeof(buffer), 0, (struct sockaddr *)&serv_udp,
                        sizeof(serv_udp));
                } else {
                    printf("UNLOCK> -6 : Operatie esuata\n");
                    fprintf(log_cl, "UNLOCK> -6 : Operatie esuata\n");
                }
            } else if (strcmp(buffer, "logout\n") == 0 && !logged) {
                printf("-1 : Clientul nu este autentificat\n");
                fprintf(log_cl, "-1 : Clientul nu este autentificat\n");
            } else if (strcmp(buffer, "logout\n") == 0 && logged) {
                logged = false;
                if (send(sock_tcp, buffer, strlen(buffer) + 1, 0) < 0) {
                    printf("Error at send\n");
                    fprintf(log_cl, "Errot at send\n");
                    fclose(log_cl);
                    exit(CALL_ERR);
                }
            } else {
                if (send(sock_tcp, buffer, strlen(buffer) + 1, 0) < 0) {
                    printf("Error at send\n");
                    fprintf(log_cl, "Error at send\n");
                    fclose(log_cl);
                    exit(CALL_ERR);
                }
            }
        }
        if (FD_ISSET(sock_udp, &read_fds)) {
            int wv = recvfrom(sock_udp, buffer, BUF_LEN, 0, (struct sockaddr *)&serv_udp, &len);
            if (wv > 0) {
                if (strcmp(buffer, "UNLOCK> T") > 0) {
                    printf("%s", buffer);
                    fprintf(log_cl, "%s", buffer);
                    memset(buffer, 0, strlen(buffer));
                    char new_buffer[9];
                    fgets(new_buffer, 9, stdin);
                    sprintf(buffer, "unlock-code %s %s", card_n, new_buffer);
                    sendto(sock_udp, buffer, sizeof(buffer), 0, (struct sockaddr *)&serv_udp,
                        sizeof(serv_udp));
                } else {
                    printf("%s", buffer);
                    fprintf(log_cl, "%s", buffer);
                }
            }
            memset(buffer, 0, BUF_LEN);
        }
    }
    return 0;
}
