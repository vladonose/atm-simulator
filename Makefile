build: server.cpp client.cpp
	g++ server.cpp -o server
	g++ client.cpp -o client

build_server: server.cpp
	g++ server.cpp -o server

build_client: client.cpp
	g++ client.cpp -o client

clean:
	rm server client

clean_logs:
	rm client_logfile* server_logfile*

clean_all:
	rm server server_logfile* client client_logfile*
