#include <unordered_map>
#include <fstream>
#include <chrono>
#include <ctime>

#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAX_CLIENTS 50
#define BUF_LEN     50
// error codes
#define INSF_ARGS   1       // insufficient arguments at runtime
#define UNAUTH      -1      // unauthenticated clients
#define OPN_SES     -2      // session already open
#define WRNG_PIN    -3      // wrong pin
#define UNKN_CN     -4      // unknown card number
#define BLK_CRD     -5      // blocked card
#define OP_FAILR    -6      // failed operation
#define NBLK_FAILR  -7      // failed card unblocking
#define INSF_FNDS   -8      // insufficient funds
#define OP_CNCL     -9      // canceled operation
#define CALL_ERR    -10     // function call error

struct User {
    char f_name[13];
    char s_name[13];
    char card_req[7];
    char pin[5];
    char s_pswd[9];

    double sold;
    double to_transfer;
    struct User *dest_transfer;
    bool blocked;
    bool logged;
    bool wait;
    bool to_unblock;
};



int main(int argc, char *argv[]) {
    char adj[20];
    char log_name[40];

    auto raw_created = std::chrono::system_clock::now();
    std::time_t created_at = std::chrono::system_clock::to_time_t(raw_created);
    strftime(adj, sizeof(adj), "%Y%m%d%X", localtime(&created_at));
    sprintf(log_name, "server_logfile%s", adj);

    FILE *log_sv = fopen(log_name, "wb");
    fprintf(log_sv, "Server logfile created at: %s\n", std::ctime(&created_at));
    if (argc < 3) {
       printf("Incorrect call;\nTry: %s <server_port> <users_data_files>\n", argv[0]);
       fprintf(log_sv, "Incorrect call;\nTry: %s <server_port> <users_data_files>\n", argv[0]);
       fclose(log_sv);
       exit(INSF_ARGS);
    }

    std::unordered_map<int, struct User> users;

    std::ifstream user_data(argv[2], std::ifstream::in);
    int n;
    user_data >> n;
    printf("Reading clients data base");
    fprintf(log_sv, "Reading clients data base");
    for (int i = 0; i < n; i++) {
        if (i == 0 || i == (n / 3) || i == (2 * n / 3)) {
            printf(".");
            fprintf(log_sv, ".");
        }
        struct User user;
        user.blocked = false;
        user.logged = false;
        user.wait = false;
        user.to_unblock = false;
        user.dest_transfer = NULL;
        user.to_transfer = 0;

        user_data >> user.f_name >> user.s_name >> user.card_req >>
                     user.pin >> user.s_pswd >> user.sold;

       users.insert(std::make_pair(atoi(user.card_req), user));
    }
    user_data.close();
    printf("\nDone!\n");
    fprintf(log_sv, "\nDone!\n");

    std::unordered_map<int, struct User>::iterator it = users.begin();
    printf("\n +--------+--------------+--------------+--------+------+----------+-----------+\n"
             " | Crt.   | First Name   | Surname      | Card # | PIN  | Secr Pwd | Sold      |\n"
             " +========+==============+==============+========+======+==========+===========+\n");
    fprintf(log_sv,
      "\n +--------+--------------+--------------+--------+------+----------+-----------+\n"
        " | Crt.   | First Name   | Surname      | Card # | PIN  | Secr Pwd | Sold      |\n"
        " +========+==============+==============+========+======+==========+===========+\n");
    int i = 1;
    for (it = users.begin(); it != users.end(); it++) {
        printf(" | %-6d | %-12s | %-12s | %-6s | %-4s | %-8s | %-9.2f |\n"
               " +--------+--------------+--------------+--------+------+----------+-----------+\n", 
                                                                        i++,
                                                                        it->second.f_name,
                                                                        it->second.s_name,
                                                                        it->second.card_req,
                                                                        it->second.pin,
                                                                        it->second.s_pswd,
                                                                        it->second.sold);
        fprintf(log_sv,
               " | %-6d | %-12s | %-12s | %-6s | %-4s | %-8s | %-9.2f |\n"
               " +--------+--------------+--------------+--------+------+----------+-----------+\n", 
                                                                        i++,
                                                                        it->second.f_name,
                                                                        it->second.s_name,
                                                                        it->second.card_req,
                                                                        it->second.pin,
                                                                        it->second.s_pswd,
                                                                        it->second.sold);
    }
    
    int server_port = atoi(argv[1]);
    printf("\nCreating clients map\n");
    fprintf(log_sv, "\nCreating clients map\n");
    n = MAX_CLIENTS + 5;
    int clients[n];
    int count_blk[n];
    int count_card[n];
    for (int i = 0; i < n; i++) {
        if (i == 0 || i == (n / 3) || i == (2 * n / 3)) {
            printf(".");
        }
        clients[i] = -1;
        count_blk[i] = 0;
        count_card[i] = -1;
    }
    printf("\nDone\n");
    fprintf(log_sv, "\nDone\n");

    int sock_udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    int sock_tcp = socket(AF_INET, SOCK_STREAM, 0);

    printf("Server UDP socket: %d\nServer TCP socket: %d\n", sock_udp, sock_tcp);
    fprintf(log_sv, "Server UDP socket: %d\nServer TCP socket: %d\n", sock_udp, sock_tcp);

    fd_set read_fds;
    fd_set tmp_fds;
    int fdmax;

    FD_ZERO(&read_fds);
    FD_ZERO(&tmp_fds);

    struct sockaddr_in serv_tcp, cl_addr;
    memset((char *) &serv_tcp, 0, sizeof(serv_tcp));
    serv_tcp.sin_family = AF_INET;
    serv_tcp.sin_port = htons(server_port);
    serv_tcp.sin_addr.s_addr = INADDR_ANY;

    struct sockaddr_in serv_udp, cl_addr_alt;
    memset((char *) &serv_udp, 0, sizeof(serv_udp));
    serv_udp.sin_family = AF_INET;
    serv_udp.sin_port = htons(server_port);
    serv_udp.sin_addr.s_addr = INADDR_ANY;

    if (bind(sock_tcp, (struct sockaddr *) &serv_tcp, sizeof(struct sockaddr)) < 0) {
        printf("Error at binding - TCP\n");
        fprintf(log_sv, "Error at binding - TCP\n");
        fclose(log_sv);
        exit(CALL_ERR);
    }

    if (bind(sock_udp, (struct sockaddr *) &serv_udp, sizeof(struct sockaddr)) < 0) {
        printf("Error at binding - UDP\n");
        fprintf(log_sv, "Error at binding - UDP\n");
        fclose(log_sv);
        exit(CALL_ERR);
    }
    
    if (listen(sock_tcp, MAX_CLIENTS) == -1) {
        printf("Error at listen\n");
        fprintf(log_sv, "Error at listen\n");
        fclose(log_sv);
        exit(CALL_ERR);
    }

    FD_SET(0, &read_fds);
    FD_SET(sock_udp, &read_fds);
    FD_SET(sock_tcp, &read_fds);
    fdmax = sock_tcp;

    char buffer[BUF_LEN];
    int cl_size, dscr;
    unsigned int len = sizeof(struct sockaddr_in);

    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 10;
    setsockopt(sock_udp, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

    for (;;) {

        tmp_fds = read_fds;

        if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) {
            printf("Error at select\n");
            fprintf(log_sv, "Error at select\n");
            fclose(log_sv);
            exit(CALL_ERR);
        }

        for (int i = sock_udp + 1; i <= fdmax; i++) {
            if (FD_ISSET(i, &tmp_fds) && i != sock_udp) {
                if (i == sock_tcp) {
                    cl_size = sizeof(cl_addr);
                    int new_sock_cl = accept(sock_tcp, (struct sockaddr *)&cl_addr, (socklen_t *)&cl_size);
                    if (new_sock_cl == -1) {
                        printf("Error at accept\n");
                        fprintf(log_sv, "Error at accept\n");
                        fclose(log_sv);
                        exit(CALL_ERR);
                    }
                    FD_SET(new_sock_cl, &read_fds);
                    if (new_sock_cl > fdmax) {
                        fdmax = new_sock_cl;
                    }
                    printf("New connection from %s, socket %d.\n", inet_ntoa(cl_addr.sin_addr), new_sock_cl);
                    fprintf(log_sv, "New connection from %s, socket %d.\n", inet_ntoa(cl_addr.sin_addr), new_sock_cl);
                } else {
                    memset(buffer, 0, BUF_LEN);
                    if ((dscr = recv(i, buffer, sizeof(buffer), 0)) <= 0) {
                        if (dscr == 0) {
                            printf("Client with socket %d hung up\n", i);
                            fprintf(log_sv, "Client with socket %d hung up\n", i);
                        } else {
                            printf("Error at recv\n");
                            fprintf(log_sv, "Error at recv\n");
                            fclose(log_sv);
                            exit(CALL_ERR);
                        }
                        clients[i] = -1;
                        count_blk[i] = 0;
                        count_card[i] = -1;
                        close(i);
                        FD_CLR(i, &read_fds);
                    } else {
                        bool wait = false;
                        printf("Client %d requesting: %s", i, buffer);
                        fprintf(log_sv, "Client %d requesting: %s", i, buffer);
                        char new_buffer[BUF_LEN];
                        memset(new_buffer, 0, BUF_LEN);
                        sprintf(new_buffer, "IBANK> ");
                        if (clients[i] != -1) {
                            std::unordered_map<int, struct User>::iterator it = users.find(clients[i]);
                            wait = it->second.wait;
                        }

                        if (!wait) {
                            char *split = strtok(buffer, " ");
                            int split_len = strlen(split);

                            // LOGIN <CARD #> <PIN>
                            if (strncmp(buffer, "login", 5) == 0 && split_len == 5) {
                                split = strtok(NULL, " ");
                                if (split != NULL) {
                                    int card_req = atoi(split);
                                    split = strtok(NULL, " ");
                                    if (split != NULL) {
                                        std::unordered_map<int, struct User>::iterator it = users.find(card_req);
                                        if (it != users.end()) {
                                            if (!(it->second.logged)) {
                                                if ((split[0] == it->second.pin[0])
                                                    && (split[1] == it->second.pin[1])
                                                    && (split[2] == it->second.pin[2])
                                                    && (split[3] == it->second.pin[3])
                                                    && (split[4] == '\n')) {
                                                    if (it->second.blocked) {
                                                        sprintf(new_buffer + strlen(new_buffer), "%d : Card blocat\n", BLK_CRD);
                                                    } else {
                                                        it->second.logged = true;
                                                        count_blk[i] = 0;
                                                        count_card[i] = -1;
                                                        clients[i] = card_req;
                                                        sprintf(new_buffer + strlen(new_buffer), "Welcome %s %s\n", it->second.f_name, it->second.s_name);
                                                    }
                                                } else {
                                                    if (count_card[i] == card_req) {
                                                        count_blk[i]++;
                                                    } else if (count_card[i] != card_req) {
                                                        count_card[i] = card_req;
                                                        count_blk[i] = 1;
                                                    }
                                                    if (count_blk[i] == 3) {
                                                        it->second.blocked = true;
                                                        count_blk[i] = 0;
                                                        sprintf(new_buffer + strlen(new_buffer), "%d : Card blocat\n", BLK_CRD);
                                                    } else {
                                                        sprintf(new_buffer + strlen(new_buffer), "%d : Pin gresit\n", WRNG_PIN);
                                                    }
                                                }
                                            } else {
                                                sprintf(new_buffer + strlen(new_buffer), "%d : Sesiune deja deschisa\n", OPN_SES);
                                            }
                                        } else {
                                            sprintf(new_buffer + strlen(new_buffer), "%d : Numar card inexistent\n", UNKN_CN);
                                        }
                                    } else {
                                        sprintf(new_buffer + strlen(new_buffer), "%d : Operatie esuata\n", OP_FAILR);
                                    }
                                } else {
                                    sprintf(new_buffer + strlen(new_buffer), "%d : Operatie esuata\n", OP_FAILR);
                                }

                            // LOGOUT
                            } else if (strncmp(buffer, "logout", 6) == 0 && split_len == 7) {
                                if (clients[i] != -1) {
                                    sprintf(new_buffer + strlen(new_buffer), "Clientul a fost deconectat\n");
                                    std::unordered_map<int, struct User>::iterator it = users.find(clients[i]);
                                    it->second.logged = false;
                                    clients[i] = -1;
                                    count_blk[i] = 0;
                                    count_card[i] = -1;
                                } else {
                                    sprintf(new_buffer + strlen(new_buffer), "%d : Clientul nu este autentificat\n", UNAUTH);
                                }

                            // LISTSOLD
                            } else if (strncmp(buffer, "listsold", 8) == 0 && split_len == 9) {
                                if (clients[i] != -1) {
                                    std::unordered_map<int, struct User>::iterator it = users.find(clients[i]);
                                    sprintf(new_buffer + strlen(new_buffer), "%.2lf\n", it->second.sold);
                                } else {
                                    sprintf(new_buffer + strlen(new_buffer), "%d : Clientul nu este autentificat\n", UNAUTH);
                                }

                            // TRANSFER <ALT_CARD #> <SUM>
                            } else if (strncmp(buffer, "transfer", 8) == 0 && split_len == 8) {
                                if (clients[i] != -1) {
                                    std::unordered_map<int, struct User>::iterator src = users.find(clients[i]);
                                    split = strtok(NULL, " ");
                                    if (split != NULL) {
                                        std::unordered_map<int, struct User>::iterator dst = users.find(atoi(split));
                                        if (dst != users.end() && dst != src) {
                                            split = strtok(NULL, " ");
                                            if (split != NULL) {
                                                double sum;
                                                sscanf(split, "%lf", &sum);
                                                if (sum <= src->second.sold) {
                                                    src->second.wait = true;
                                                    src->second.to_transfer = sum;
                                                    src->second.dest_transfer = &dst->second;
                                                    sprintf(new_buffer + strlen(new_buffer), "Transfer %.2lf catre %s %s? [y/n]\n",
                                                                                        sum, dst->second.f_name, dst->second.s_name);
                                                } else {
                                                    sprintf(new_buffer + strlen(new_buffer), "%d : Fonduri insuficiente\n",INSF_FNDS);
                                                }
                                            } else {
                                                sprintf(new_buffer + strlen(new_buffer), "%d : Operatie esuata\n", OP_FAILR);
                                            }
                                        } else {
                                            sprintf(new_buffer + strlen(new_buffer), "%d : Operatie esuata\n", OP_FAILR);
                                        }
                                    } else {
                                        sprintf(new_buffer + strlen(new_buffer), "%d : Operatie esuata\n", OP_FAILR);
                                    }
                                } else {
                                    sprintf(new_buffer + strlen(new_buffer), "%d : Clientul nu este autentificat\n", UNAUTH);
                                }

                            // QUIT
                            } else if (strncmp(buffer, "quit", 4) == 0 && split_len == 5) {
                                clients[i] = -1;
                                count_blk[i] = 0;
                                count_card[i] = -1;
                                FD_CLR(i, &read_fds);
                                close(i);
                            } else {
                                if (strcmp(buffer, "\n") != 0)
                                    sprintf(new_buffer + strlen(new_buffer), "%d : Operatie esuata\n", OP_FAILR);
                            }
                        } else {
                            std::unordered_map<int, struct User>::iterator it = users.find(clients[i]);
                            if (buffer[0] == 'y') {
                                it->second.dest_transfer->sold += it->second.to_transfer;
                                it->second.sold -= it->second.to_transfer;
                                sprintf(new_buffer + strlen(new_buffer), "Transfer realizat cu succes\n");
                            } else {
                                sprintf(new_buffer + strlen(new_buffer), "%d : Operatie anulata\n", OP_CNCL);
                            }
                            it->second.to_transfer = 0;
                            it->second.wait = false;
                            it->second.dest_transfer = NULL;
                        }
                        if (strcmp(buffer, "\n") != 0 && FD_ISSET(i, &read_fds)) {
                            if (send(i, new_buffer, strlen(new_buffer) + 1, 0) < 0) {
                                printf("Error at send\n");
                                fprintf(log_sv, "Error at send\n");
                                fclose(log_sv);
                                exit(CALL_ERR);
                            }
                        }
                    }
                }
            }
        }

        if (FD_ISSET(0, &tmp_fds)) {
            memset(buffer, 0 , BUF_LEN);
            fgets(buffer, BUF_LEN, stdin);
            fprintf(log_sv, "Server command: %s", buffer);
            // QUIT
            if (strncmp(buffer, "quit", 4) == 0) {
                memset(buffer, 0, BUF_LEN);
                sprintf(buffer, "Server is shutting down\n");
                for (int i = sock_tcp + 1; i <= fdmax; i++) {
                    if (FD_ISSET(i, &read_fds)) {
                        if (send(i, buffer, strlen(buffer) + 1, 0) < 0) {
                            printf("Error at send\n");
                            fprintf(log_sv, "Error at send\n");
                            fclose(log_sv);
                            exit(CALL_ERR);
                        }
                        close(i);
                    }
                }
                close(sock_tcp);
                return 0;
            } else {
                printf("Unknown command\n");
                fprintf(log_sv, "Unknown command\n");
            }
        }

        if (FD_ISSET(sock_udp, &read_fds)) {
            int wv = recvfrom(sock_udp, buffer, BUF_LEN, 0, (struct sockaddr *)&cl_addr_alt, &len);
            if (wv > 0) {
                if (strncmp(buffer, "unlock", 6) == 0 && strncmp(buffer, "unlock-", 7) != 0) {
                    char *split = strtok(buffer, " ");
                    split = strtok(NULL, " ");
                    if (split != NULL) {
                        int card_n = atoi(split);
                        std::unordered_map<int, struct User>::iterator it = users.find(card_n);
                        if (it != users.end()) {
                            memset(buffer, 0, BUF_LEN);
                            if (it->second.blocked) {
                                sprintf(buffer, "UNLOCK> Trimite parola secreta\n");
                            } else {
                                sprintf(buffer, "UNLOCK> %d : Operatie esuata\n", OP_FAILR);
                            }
                        } else {
                            memset(buffer, 0, BUF_LEN);
                            sprintf(buffer, "UNLOCK> %d : Numar card inexistent\n", UNKN_CN);
                        }
                    } else {
                        memset(buffer, 0, BUF_LEN);
                        sprintf(buffer, "UNLOCK> %d : Operatie esuata\n", OP_FAILR);
                    }
                } else if (strncmp(buffer, "unlock-code", 11) == 0) {
                    char *split = strtok(buffer, " ");
                    split = strtok(NULL, " ");
                    int card_n = atoi(split);
                    std::unordered_map<int, struct User>::iterator it = users.find(card_n);
                    split = strtok(NULL, " ");
                    if (strncmp(split, it->second.s_pswd, strlen(it->second.s_pswd)) == 0
                        && strlen(split) == strlen(it->second.s_pswd)) {
                        it->second.blocked = false;
                        memset(buffer, 0, BUF_LEN);
                        sprintf(buffer, "UNLOCK> Card deblocat\n");
                    } else {
                        memset(buffer, 0, BUF_LEN);
                        sprintf(buffer, "UNLOCK> %d : Deblocare esuata\n", NBLK_FAILR);
                    }
                }
                sendto(sock_udp, buffer, sizeof(buffer), 0, (struct sockaddr *)&cl_addr_alt,
                        sizeof(serv_udp));
            }
        }
    }

    return 0;
}